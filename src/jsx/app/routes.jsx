/* Hack to pass parmeters to a route */

var wrapComponent = function(Component, props) {
  return React.createClass({
    render: function() {
      return React.createElement(Component, props);
    }
  });
};

/* ERROR PAGES */
var notfound = require('./routes/notfound.jsx');

/* APP PAGES */
var blank = require('./routes/app/blank.jsx');
var accounts = require('./routes/app/accounts/index.jsx');
var account_new = require('./routes/app/accounts/new.jsx');
var orders = require('./routes/app/orders/index.jsx');
var order_new = require('./routes/app/orders/new.jsx');

/* ROUTES */
module.exports = (
  <Route handler={ReactRouter.RouteHandler}>
    <DefaultRoute handler={blank} />
    <Route path='/' handler={blank} />
    <NotFoundRoute handler={notfound} />
    <Route name="accounts" path='/accounts' handler={accounts} />
    <Route path="account/new" handler={account_new} />
    <Route name="orders"  path='/orders' handler={orders} />
    <Route path="order/new" handler={order_new} />
    <Route name="orders_pending"  path='/orders/pending' handler={wrapComponent(orders, {status: "pending"})} />
    <Route name="orders_approved"  path='/orders/approved' handler={wrapComponent(orders, {status: "approved"})} />
    <Route name="orders_shipped"  path='/orders/shipped' handler={wrapComponent(orders, {status: "shipped"})} />
    <Route name="orders_completed"  path='/orders/completed' handler={wrapComponent(orders, {status: "completed"})} />
    <Route name="orders_canceled"  path='/orders/canceled' handler={wrapComponent(orders, {status: "canceled"})} />
  </Route>
);
