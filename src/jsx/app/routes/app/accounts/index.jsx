var Header = require('../../../common/header.jsx');
var Sidebar = require('../../../common/sidebar.jsx');
var Footer = require('../../../common/footer.jsx');

var Body = React.createClass({
  loadDataFromServer: function() {
    $.get("http://localhost:5000/api/accounts", function(result) {
      var accounts = result["account_indices"];
      if (this.isMounted()) {
        this.setState({
          accounts: accounts
        });
      }
    }.bind(this));
  },
  componentDidMount: function() {
    this.loadDataFromServer();
  },
  componentDidUpdate: function(){
    $('#example')
      .addClass('nowrap')
      .dataTable({
        sDom: '<"top"flp<"clear">>rt<"bottom"ip<"clear">>',
        pageLength: 25,
        responsive: true
    });
 	},
  getInitialState: function() {
    return {accounts: []};
  },
  render: function() {
    var accountRows = this.state.accounts.map(function (account, index) {
      return (
        <tr key={index}>
          <td>{account.name}</td>
          <td>{account.salesperson}</td>
          <td>{account.territory}</td>
        </tr>
      );
    });
    return (
      <Container id='body'>
        <Grid>
          <Row>
            <Col xs={12}>
              <PanelContainer>
                <Panel>
                  <PanelBody>
                    <Grid>
                      <Row>
                        <Col xs={12}>
                          <Table id='example' className='display' cellSpacing='0' width='100%'>
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Account Manager</th>
                                <th>Territory</th>
                              </tr>
                            </thead>
                            <tfoot>
                              <tr>
                                <th>Name</th>
                                <th>Account Manager</th>
                                <th>Territory</th>
                              </tr>
                            </tfoot>
                            <tbody>
                              {accountRows}
                            </tbody>
                          </Table>
                          <br/>
                        </Col>
                      </Row>
                    </Grid>
                  </PanelBody>
                </Panel>
              </PanelContainer>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
});

var classSet = React.addons.classSet;
var accountTables = React.createClass({
  mixins: [SidebarMixin],
  render: function() {
    var classes = classSet({
      'container-open': this.state.open
    });
    return (
      <Container id='container' className={classes}>
        <Sidebar />
        <Header />
        <Body />
        <Footer />
      </Container>
    );
  }
});

module.exports = accountTables;
