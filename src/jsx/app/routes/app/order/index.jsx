var Header = require('../../common/header.jsx');
var Sidebar = require('../../common/sidebar.jsx');
var Footer = require('../../common/footer.jsx');

var Body = React.createClass({
  loadDataFromServer: function() {
    $.get("http://localhost:5000/api/orders", "status="+ this.props.status ,function(result) {
      var orders = result["order_indices"];
      if (this.isMounted()) {
        this.setState({
          orders: orders
        });
      }
    }.bind(this));
  },
  componentDidMount: function() {
    this.loadDataFromServer();
  },
  componentDidUpdate: function(){
    $('#example')
      .addClass('nowrap')
      .dataTable({
        sDom: '<"top"flp<"clear">>rt<"bottom"ip<"clear">>',
        pageLength: 25,
        responsive: true
    });
 	},
  getInitialState: function() {
    return {orders: []};
  },
  render: function() {
    var orderRows = this.state.orders.map(function (order, index) {
      return (
        <tr key={index}>
          <td>{order.account}</td>
          <td>{order.salesperson}</td>
          <td>{order.requesteddate}</td>
        </tr>
      );
    });
    return (
      <Container id='body'>
        <Grid>
          <Row>
            <Col xs={12}>
              <PanelContainer>
                <Panel>
                  <PanelBody>
                    <Grid>
                      <Row>
                        <Col xs={12}>
                          <Table id='example' className='display' cellSpacing='0' width='100%'>
                            <thead>
                              <tr>
                                <th>Account</th>
                                <th>Account Manager</th>
                                <th>Request Delivery Date</th>
                              </tr>
                            </thead>
                            <tfoot>
                              <tr>
                                <th>Account</th>
                                <th>Account Manager</th>
                                <th>Submitted At</th>
                              </tr>
                            </tfoot>
                            <tbody>
                              {orderRows}
                            </tbody>
                          </Table>
                          <br/>
                        </Col>
                      </Row>
                    </Grid>
                  </PanelBody>
                </Panel>
              </PanelContainer>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
});

var classSet = React.addons.classSet;
var orderTables = React.createClass({
  mixins: [SidebarMixin],
  render: function() {
    var classes = classSet({
      'container-open': this.state.open
    });
    return (
      <Container id='container' className={classes}>
        <Sidebar />
        <Header />
        <Body status={this.props.status} />
        <Footer />
      </Container>
    );
  }
});

module.exports = orderTables;
