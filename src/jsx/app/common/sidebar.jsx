var ApplicationSidebar = React.createClass({
  getBadgeCounts: function() {
    $.get("http://localhost:5000/api/orders", "status=pending" ,function(result) {
      var orders = result["order_indices"];
      if (this.isMounted()) {
        this.setState({
          order_count: orders.length
        });
      }
    }.bind(this));
  },
  componentDidMount: function() {
    this.getBadgeCounts();
  },
  getInitialState: function() {
    return {order_count: "?"};
  },
  render: function() {
    return (
      <div>
        <Grid>
          <Row>
            <Col xs={12}>
              <div className='sidebar-nav-container'>
                <SidebarNav style={{marginBottom: 0}}>
                  <SidebarNavItem glyph='icon-fontello-users' name='Accounts'>
                    <SidebarNav>
                      <SidebarNavItem href='/account/new' glyph='icon-fontello-user' name='Create New Account' />
                      <SidebarNavItem href='/accounts' glyph='icon-fontello-eye' name='Accounts' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem glyph='icon-fontello-newspaper' name={<span>Orders <BLabel className='bg-darkgreen45 fg-white'>{this.state.order_count}</BLabel></span>}>
                    <SidebarNav>
                      <SidebarNavItem href='/order/new' glyph='icon-fontello-edit' name='Create New Order' />
                      <SidebarNavItem href='/orders/pending' glyph='icon-fontello-eye' name={<span>Pending Orders <BLabel className='bg-darkgreen45 fg-white'>{this.state.order_count}</BLabel></span>} />
                      <SidebarNavItem href='/orders/approved' glyph='icon-fontello-eye' name='Approved Orders' />
                      <SidebarNavItem href='/orders/shipped' glyph='icon-fontello-eye' name='Shipped Orders' />
                      <SidebarNavItem href='/orders/completed' glyph='icon-fontello-eye' name='Completed Orders' />
                      <SidebarNavItem href='/orders/canceled' glyph='icon-fontello-eye' name='Canceled Orders' />
                      <SidebarNavItem href='/orders' glyph='icon-fontello-search' name='All Orders' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem glyph='icon-feather-mail' name={<span>Production <BLabel className='bg-darkgreen45 fg-white'>3</BLabel></span>}>
                    <SidebarNav>
                      <SidebarNavItem glyph='icon-feather-inbox' name='Inbox' />
                      <SidebarNavItem glyph='icon-outlined-mail-open' name='Mail' />
                      <SidebarNavItem glyph='icon-dripicons-message' name='Compose' />
                    </SidebarNav>
                  </SidebarNavItem>
                  <SidebarNavItem glyph='icon-feather-mail' name={<span>Reporting <BLabel className='bg-darkgreen45 fg-white'>3</BLabel></span>}>
                    <SidebarNav>
                      <SidebarNavItem glyph='icon-feather-inbox' name='Inbox' />
                      <SidebarNavItem glyph='icon-outlined-mail-open' name='Mail' />
                      <SidebarNavItem glyph='icon-dripicons-message' name='Compose' />
                    </SidebarNav>
                  </SidebarNavItem>

                </SidebarNav>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
});

var DummySidebar = React.createClass({
  render: function() {
    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <div className='sidebar-header'>DUMMY SIDEBAR</div>
              <span>Fort Point Camber</span>
          </Col>
        </Row>
      </Grid>
    );
  }
});

var SidebarSection = React.createClass({
  render: function() {
    return (
      <div id='sidebar' {...this.props}>
        <div id='avatar'>
          <Grid>
            <Row className='fg-white'>
              <Col xs={4} collapseRight>
                <img src='/imgs/avatars/avatar8.png' width='40' height='40' />
              </Col>
              <Col xs={8} collapseLeft id='avatar-col'>
                <div style={{top: 23, fontSize: 16, lineHeight: 1, position: 'relative'}}>Justin Catalana</div>
                {/*<div>
                  <Progress id='demo-progress' value={30} min={0} max={100} color='#ffffff'/>
                  <a href='#'><Icon id='demo-icon' bundle='fontello' glyph='lock-5' /></a>
                </div>*/}
              </Col>
            </Row>
          </Grid>
        </div>
{/*        <SidebarControls>
          <SidebarControlBtn bundle='fontello' glyph='docs' sidebar={0} />
          <SidebarControlBtn bundle='fontello' glyph='chat-1' sidebar={1} />
          <SidebarControlBtn bundle='fontello' glyph='chart-pie-2' sidebar={2} />
          <SidebarControlBtn bundle='fontello' glyph='th-list-2' sidebar={3} />
          <SidebarControlBtn bundle='fontello' glyph='bell-5' sidebar={4} />
        </SidebarControls>
*/}        <div id='sidebar-container'>
          <Sidebar sidebar={0} active>
            <ApplicationSidebar />
          </Sidebar>
          <Sidebar sidebar={1}>
            <DummySidebar />
          </Sidebar>
          <Sidebar sidebar={2}>
            <DummySidebar />
          </Sidebar>
          <Sidebar sidebar={3}>
            <DummySidebar />
          </Sidebar>
          <Sidebar sidebar={4}>
            <DummySidebar />
          </Sidebar>
        </div>
      </div>
    );
  }
});

module.exports = SidebarSection;
